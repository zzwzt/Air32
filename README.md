# 基于Air32的手持示波器

#### 介绍
本项目硬件部分使用立创EDA进行设计，软件部分使用了FreeRTOS和LVGL图形库，适合初学者学习。
已经在立创开源广场开源。

#### 使用说明

本示波器仅用于学习使用，未经本人授权，禁止商用。
如发现有BUG，或者需要做好的板子，加QQ：2951700533


程序有很多写的不好的地方，大家可以自由修改

#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
